EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title "Velux Shutter Control"
Date "2021-01-03"
Rev "1"
Comp "David Hruby"
Comment1 "Relays Control"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CustomLib:HK19F-DC5V-SHGY_ K?
U 1 1 5FF27FBB
P 1375 3275
AR Path="/5FF27FBB" Ref="K?"  Part="1" 
AR Path="/5FF237A6/5FF27FBB" Ref="K11"  Part="1" 
F 0 "K11" V 2450 3500 50  0000 R CNN
F 1 "HK19F-DC5V-SHGY_" H 2450 2825 50  0000 R CNN
F 2 "CustomLib:HK19FDC5VSHGY" H 2525 3375 50  0001 L CNN
F 3 "https://datasheet.lcsc.com/szlcsc/Ningbo-Keke-New-Era-Appliance-HK19F-DC5V-SHG-Y_C54512.pdf" H 2525 3275 50  0001 L CNN
F 4 "Relays General Purpose Non Latching 5VDC DPDT 1A (125VAC , 24VDC) Max Double knives10*20 RoHS" H 2525 3175 50  0001 L CNN "Description"
F 5 "12.3" H 2525 3075 50  0001 L CNN "Height"
F 6 "HUIKE" H 2525 2975 50  0001 L CNN "Manufacturer_Name"
F 7 "HK19F-DC5V-SHG（Y)" H 2525 2875 50  0001 L CNN "Manufacturer_Part_Number"
	1    1375 3275
	0    -1   -1   0   
$EndComp
$Comp
L CustomLib:G5NB-1A-E-DC5 K?
U 1 1 5FF27FC7
P 3000 3250
AR Path="/5FF27FC7" Ref="K?"  Part="1" 
AR Path="/5FF237A6/5FF27FC7" Ref="K12"  Part="1" 
F 0 "K12" V 4075 3375 50  0000 L CNN
F 1 "G5NB-1A-E-DC5" H 3325 2875 50  0000 L CNN
F 2 "CustomLib:G5NB1A4DC5" H 4150 3350 50  0001 L CNN
F 3 "https://omronfs.omron.com/en_US/ecb/products/pdf/en-g5nb.pdf" H 4150 3250 50  0001 L CNN
F 4 "General Purpose Relays SPST-NO Flux-Tight 5VDC High-Capacity" H 4150 3150 50  0001 L CNN "Description"
F 5 "15" H 4150 3050 50  0001 L CNN "Height"
F 6 "Omron Electronics" H 4150 2950 50  0001 L CNN "Manufacturer_Name"
F 7 "G5NB-1A-E-DC5" H 4150 2850 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "653-G5NB-1A-E-DC5" H 4150 2550 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Omron-Electronics/G5NB-1A-E-DC5?qs=3jhwTwe4bOEZ3SI7aFLd0Q%3D%3D" H 4150 2450 50  0001 L CNN "Mouser Price/Stock"
	1    3000 3250
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J?
U 1 1 5FF27FCD
P 4450 2550
AR Path="/5FF27FCD" Ref="J?"  Part="1" 
AR Path="/5FF237A6/5FF27FCD" Ref="J1"  Part="1" 
F 0 "J1" V 4322 2630 50  0000 L CNN
F 1 "Screw_Terminal_01x02" V 4413 2630 50  0000 L CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 4450 2550 50  0001 C CNN
F 3 "~" H 4450 2550 50  0001 C CNN
	1    4450 2550
	1    0    0    -1  
$EndComp
$Comp
L Device:Ferrite_Bead FB12
U 1 1 5FF28F5B
P 3975 2975
F 0 "FB12" H 4112 3021 50  0000 L CNN
F 1 "Ferrite_Bead" H 4112 2930 50  0000 L CNN
F 2 "Ferrite_THT:LairdTech_28C0236-0JW-10" V 3905 2975 50  0001 C CNN
F 3 "~" H 3975 2975 50  0001 C CNN
	1    3975 2975
	0    -1   -1   0   
$EndComp
$Comp
L Device:Ferrite_Bead FB11
U 1 1 5FF29952
P 3975 2300
F 0 "FB11" H 4112 2346 50  0000 L CNN
F 1 "Ferrite_Bead" H 4112 2255 50  0000 L CNN
F 2 "Ferrite_THT:LairdTech_28C0236-0JW-10" V 3905 2300 50  0001 C CNN
F 3 "~" H 3975 2300 50  0001 C CNN
	1    3975 2300
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4007 D12
U 1 1 5FF2CD02
P 2625 2675
F 0 "D12" H 2575 2800 50  0000 L CNN
F 1 "1N4007" H 2500 2575 50  0000 L CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 2625 2500 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88503/1n4001.pdf" H 2625 2675 50  0001 C CNN
	1    2625 2675
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4007 D11
U 1 1 5FF2E785
P 1025 2575
F 0 "D11" H 975 2700 50  0000 L CNN
F 1 "1N4007" H 900 2475 50  0000 L CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 1025 2400 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88503/1n4001.pdf" H 1025 2575 50  0001 C CNN
	1    1025 2575
	0    -1   -1   0   
$EndComp
$Comp
L dk_Transistors-Bipolar-BJT-Single:BC33725TA Q11
U 1 1 5FF4DB32
P 1175 1625
F 0 "Q11" H 1075 1450 60  0000 L CNN
F 1 "BC33725TA" V 1425 1275 60  0000 L CNN
F 2 "digikey-footprints:TO-92-3_Formed_Leads" H 1375 1825 60  0001 L CNN
F 3 "https://www.onsemi.com/pub/Collateral/BC338-D.pdf" H 1375 1925 60  0001 L CNN
F 4 "BC33725TACT-ND" H 1375 2025 60  0001 L CNN "Digi-Key_PN"
F 5 "BC33725TA" H 1375 2125 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 1375 2225 60  0001 L CNN "Category"
F 7 "Transistors - Bipolar (BJT) - Single" H 1375 2325 60  0001 L CNN "Family"
F 8 "https://www.onsemi.com/pub/Collateral/BC338-D.pdf" H 1375 2425 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/on-semiconductor/BC33725TA/BC33725TACT-ND/1532787" H 1375 2525 60  0001 L CNN "DK_Detail_Page"
F 10 "TRANS NPN 45V 0.8A TO-92" H 1375 2625 60  0001 L CNN "Description"
F 11 "ON Semiconductor" H 1375 2725 60  0001 L CNN "Manufacturer"
F 12 "Active" H 1375 2825 60  0001 L CNN "Status"
	1    1175 1625
	0    1    1    0   
$EndComp
$Comp
L Device:R R11e1
U 1 1 5FF4E5B0
P 975 1275
F 0 "R11e1" V 900 1375 50  0000 R CNN
F 1 "10kΩ" V 1075 1375 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 905 1275 50  0001 C CNN
F 3 "~" H 975 1275 50  0001 C CNN
	1    975  1275
	0    1    1    0   
$EndComp
$Comp
L Device:R R11b1
U 1 1 5FF4ECBC
P 1175 975
F 0 "R11b1" V 1250 925 50  0000 C CNN
F 1 "10kΩ" V 1075 975 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 1105 975 50  0001 C CNN
F 3 "~" H 1175 975 50  0001 C CNN
	1    1175 975 
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5FF5A68F
P 775 1725
F 0 "#PWR0101" H 775 1475 50  0001 C CNN
F 1 "GND" H 780 1552 50  0000 C CNN
F 2 "" H 775 1725 50  0001 C CNN
F 3 "" H 775 1725 50  0001 C CNN
	1    775  1725
	0    1    1    0   
$EndComp
Wire Wire Line
	1025 3275 1375 3275
Wire Wire Line
	1175 1425 1175 1275
Wire Wire Line
	1175 1275 1125 1275
Wire Wire Line
	1175 1275 1175 1125
Connection ~ 1175 1275
Wire Wire Line
	775  1275 825  1275
Wire Wire Line
	775  1725 975  1725
Connection ~ 775  1725
Wire Wire Line
	1025 2425 1025 1975
Wire Wire Line
	1025 1975 1375 1975
Text GLabel 775  2025 3    50   Input ~ 0
GND
Text GLabel 925  3275 0    50   Input ~ 0
+24V
Wire Wire Line
	925  3275 1025 3275
Connection ~ 1025 3275
Wire Wire Line
	775  2025 775  1725
Wire Wire Line
	1375 1975 1375 1725
Connection ~ 1375 1975
Wire Wire Line
	1175 825  1175 700 
Text HLabel 1175 700  1    50   Input ~ 0
SD1
Wire Wire Line
	2625 2825 2625 3250
Wire Wire Line
	2625 2525 2625 1950
Wire Wire Line
	2625 1950 3000 1950
$Comp
L dk_Transistors-Bipolar-BJT-Single:BC33725TA Q12
U 1 1 5FF7E97E
P 2800 1600
F 0 "Q12" H 2700 1425 60  0000 L CNN
F 1 "BC33725TA" V 3050 1250 60  0000 L CNN
F 2 "digikey-footprints:TO-92-3_Formed_Leads" H 3000 1800 60  0001 L CNN
F 3 "https://www.onsemi.com/pub/Collateral/BC338-D.pdf" H 3000 1900 60  0001 L CNN
F 4 "BC33725TACT-ND" H 3000 2000 60  0001 L CNN "Digi-Key_PN"
F 5 "BC33725TA" H 3000 2100 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 3000 2200 60  0001 L CNN "Category"
F 7 "Transistors - Bipolar (BJT) - Single" H 3000 2300 60  0001 L CNN "Family"
F 8 "https://www.onsemi.com/pub/Collateral/BC338-D.pdf" H 3000 2400 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/on-semiconductor/BC33725TA/BC33725TACT-ND/1532787" H 3000 2500 60  0001 L CNN "DK_Detail_Page"
F 10 "TRANS NPN 45V 0.8A TO-92" H 3000 2600 60  0001 L CNN "Description"
F 11 "ON Semiconductor" H 3000 2700 60  0001 L CNN "Manufacturer"
F 12 "Active" H 3000 2800 60  0001 L CNN "Status"
	1    2800 1600
	0    1    1    0   
$EndComp
Wire Wire Line
	3000 1950 3000 1700
Connection ~ 3000 1950
Text GLabel 2525 3250 0    50   Input ~ 0
+24V
Wire Wire Line
	2525 3250 2625 3250
Connection ~ 2625 3250
Wire Wire Line
	2625 3250 3000 3250
Text GLabel 1475 3425 3    50   Input ~ 0
+24V
Text GLabel 1475 1775 1    50   Input ~ 0
GND
Wire Wire Line
	1575 1975 1575 1825
Wire Wire Line
	1675 3275 1950 3275
Wire Wire Line
	1950 3275 1950 1825
Wire Wire Line
	1950 1825 1575 1825
Wire Wire Line
	1675 1975 2075 1975
Wire Wire Line
	2075 1975 2075 3350
Wire Wire Line
	2075 3350 1575 3350
Connection ~ 1575 3350
Wire Wire Line
	1575 3350 1575 3275
$Comp
L Device:R R12e1
U 1 1 5FF8585F
P 2550 1250
F 0 "R12e1" V 2475 1350 50  0000 R CNN
F 1 "10kΩ" V 2650 1350 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 2480 1250 50  0001 C CNN
F 3 "~" H 2550 1250 50  0001 C CNN
	1    2550 1250
	0    1    1    0   
$EndComp
$Comp
L Device:R R12b1
U 1 1 5FF85E2A
P 2800 975
F 0 "R12b1" V 2875 925 50  0000 C CNN
F 1 "10kΩ" V 2700 975 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 2730 975 50  0001 C CNN
F 3 "~" H 2800 975 50  0001 C CNN
	1    2800 975 
	-1   0    0    1   
$EndComp
Wire Wire Line
	2700 1250 2800 1250
Wire Wire Line
	2800 1400 2800 1250
Connection ~ 2800 1250
Wire Wire Line
	2800 1250 2800 1125
Wire Wire Line
	2800 825  2800 700 
Text HLabel 2800 700  1    50   Input ~ 0
SM1
Wire Wire Line
	2600 1700 2325 1700
Wire Wire Line
	2325 1700 2325 1250
Wire Wire Line
	2325 1250 2400 1250
Text GLabel 2325 2000 3    50   Input ~ 0
GND
Wire Wire Line
	2325 2000 2325 1700
Connection ~ 2325 1700
$Comp
L power:GND #PWR0102
U 1 1 5FF89EDE
P 2325 1700
F 0 "#PWR0102" H 2325 1450 50  0001 C CNN
F 1 "GND" H 2330 1527 50  0000 C CNN
F 2 "" H 2325 1700 50  0001 C CNN
F 3 "" H 2325 1700 50  0001 C CNN
	1    2325 1700
	0    1    1    0   
$EndComp
$Comp
L power:+24V #PWR0103
U 1 1 5FF8A75A
P 1025 3275
F 0 "#PWR0103" H 1025 3125 50  0001 C CNN
F 1 "+24V" V 1040 3403 50  0000 L CNN
F 2 "" H 1025 3275 50  0001 C CNN
F 3 "" H 1025 3275 50  0001 C CNN
	1    1025 3275
	-1   0    0    1   
$EndComp
$Comp
L power:+24V #PWR0104
U 1 1 5FF8B33A
P 2625 3250
F 0 "#PWR0104" H 2625 3100 50  0001 C CNN
F 1 "+24V" V 2640 3378 50  0000 L CNN
F 2 "" H 2625 3250 50  0001 C CNN
F 3 "" H 2625 3250 50  0001 C CNN
	1    2625 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	3825 3425 3825 2975
Wire Wire Line
	3200 3425 3200 3250
Wire Wire Line
	4125 2975 4125 2650
Wire Wire Line
	4125 2650 4250 2650
Wire Wire Line
	4250 2550 4125 2550
Wire Wire Line
	4125 2550 4125 2300
Wire Wire Line
	1475 3425 1475 3275
Wire Wire Line
	1475 1975 1475 1775
Wire Wire Line
	3100 3600 3100 3250
Wire Wire Line
	3750 3650 3750 2300
Wire Wire Line
	3750 2300 3825 2300
$Comp
L CustomLib:HK19F-DC5V-SHGY_ K?
U 1 1 5FF86EB5
P 5600 3275
AR Path="/5FF86EB5" Ref="K?"  Part="1" 
AR Path="/5FF237A6/5FF86EB5" Ref="K21"  Part="1" 
F 0 "K21" V 6675 3500 50  0000 R CNN
F 1 "HK19F-DC5V-SHGY_" H 6675 2825 50  0000 R CNN
F 2 "CustomLib:HK19FDC5VSHGY" H 6750 3375 50  0001 L CNN
F 3 "https://datasheet.lcsc.com/szlcsc/Ningbo-Keke-New-Era-Appliance-HK19F-DC5V-SHG-Y_C54512.pdf" H 6750 3275 50  0001 L CNN
F 4 "Relays General Purpose Non Latching 5VDC DPDT 1A (125VAC , 24VDC) Max Double knives10*20 RoHS" H 6750 3175 50  0001 L CNN "Description"
F 5 "12.3" H 6750 3075 50  0001 L CNN "Height"
F 6 "HUIKE" H 6750 2975 50  0001 L CNN "Manufacturer_Name"
F 7 "HK19F-DC5V-SHG（Y)" H 6750 2875 50  0001 L CNN "Manufacturer_Part_Number"
	1    5600 3275
	0    -1   -1   0   
$EndComp
$Comp
L CustomLib:G5NB-1A-E-DC5 K?
U 1 1 5FF86EC1
P 7225 3250
AR Path="/5FF86EC1" Ref="K?"  Part="1" 
AR Path="/5FF237A6/5FF86EC1" Ref="K22"  Part="1" 
F 0 "K22" V 8300 3375 50  0000 L CNN
F 1 "G5NB-1A-E-DC5" H 7550 2875 50  0000 L CNN
F 2 "CustomLib:G5NB1A4DC5" H 8375 3350 50  0001 L CNN
F 3 "https://omronfs.omron.com/en_US/ecb/products/pdf/en-g5nb.pdf" H 8375 3250 50  0001 L CNN
F 4 "General Purpose Relays SPST-NO Flux-Tight 5VDC High-Capacity" H 8375 3150 50  0001 L CNN "Description"
F 5 "15" H 8375 3050 50  0001 L CNN "Height"
F 6 "Omron Electronics" H 8375 2950 50  0001 L CNN "Manufacturer_Name"
F 7 "G5NB-1A-E-DC5" H 8375 2850 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "653-G5NB-1A-E-DC5" H 8375 2550 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Omron-Electronics/G5NB-1A-E-DC5?qs=3jhwTwe4bOEZ3SI7aFLd0Q%3D%3D" H 8375 2450 50  0001 L CNN "Mouser Price/Stock"
	1    7225 3250
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J?
U 1 1 5FF86EC7
P 8675 2550
AR Path="/5FF86EC7" Ref="J?"  Part="1" 
AR Path="/5FF237A6/5FF86EC7" Ref="J2"  Part="1" 
F 0 "J2" V 8547 2630 50  0000 L CNN
F 1 "Screw_Terminal_01x02" V 8638 2630 50  0000 L CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 8675 2550 50  0001 C CNN
F 3 "~" H 8675 2550 50  0001 C CNN
	1    8675 2550
	1    0    0    -1  
$EndComp
$Comp
L Device:Ferrite_Bead FB22
U 1 1 5FF86ECD
P 8200 2975
F 0 "FB22" H 8337 3021 50  0000 L CNN
F 1 "Ferrite_Bead" H 8337 2930 50  0000 L CNN
F 2 "Ferrite_THT:LairdTech_28C0236-0JW-10" V 8130 2975 50  0001 C CNN
F 3 "~" H 8200 2975 50  0001 C CNN
	1    8200 2975
	0    -1   -1   0   
$EndComp
$Comp
L Device:Ferrite_Bead FB21
U 1 1 5FF86ED3
P 8200 2300
F 0 "FB21" H 8337 2346 50  0000 L CNN
F 1 "Ferrite_Bead" H 8337 2255 50  0000 L CNN
F 2 "Ferrite_THT:LairdTech_28C0236-0JW-10" V 8130 2300 50  0001 C CNN
F 3 "~" H 8200 2300 50  0001 C CNN
	1    8200 2300
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4007 D22
U 1 1 5FF86ED9
P 6850 2675
F 0 "D22" H 6800 2800 50  0000 L CNN
F 1 "1N4007" H 6725 2575 50  0000 L CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 6850 2500 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88503/1n4001.pdf" H 6850 2675 50  0001 C CNN
	1    6850 2675
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4007 D21
U 1 1 5FF86EDF
P 5250 2575
F 0 "D21" H 5200 2700 50  0000 L CNN
F 1 "1N4007" H 5125 2475 50  0000 L CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 5250 2400 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88503/1n4001.pdf" H 5250 2575 50  0001 C CNN
	1    5250 2575
	0    -1   -1   0   
$EndComp
$Comp
L dk_Transistors-Bipolar-BJT-Single:BC33725TA Q21
U 1 1 5FF86EEE
P 5400 1625
F 0 "Q21" H 5300 1450 60  0000 L CNN
F 1 "BC33725TA" V 5650 1275 60  0000 L CNN
F 2 "digikey-footprints:TO-92-3_Formed_Leads" H 5600 1825 60  0001 L CNN
F 3 "https://www.onsemi.com/pub/Collateral/BC338-D.pdf" H 5600 1925 60  0001 L CNN
F 4 "BC33725TACT-ND" H 5600 2025 60  0001 L CNN "Digi-Key_PN"
F 5 "BC33725TA" H 5600 2125 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 5600 2225 60  0001 L CNN "Category"
F 7 "Transistors - Bipolar (BJT) - Single" H 5600 2325 60  0001 L CNN "Family"
F 8 "https://www.onsemi.com/pub/Collateral/BC338-D.pdf" H 5600 2425 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/on-semiconductor/BC33725TA/BC33725TACT-ND/1532787" H 5600 2525 60  0001 L CNN "DK_Detail_Page"
F 10 "TRANS NPN 45V 0.8A TO-92" H 5600 2625 60  0001 L CNN "Description"
F 11 "ON Semiconductor" H 5600 2725 60  0001 L CNN "Manufacturer"
F 12 "Active" H 5600 2825 60  0001 L CNN "Status"
	1    5400 1625
	0    1    1    0   
$EndComp
$Comp
L Device:R R21e1
U 1 1 5FF86EF4
P 5200 1275
F 0 "R21e1" H 5130 1229 50  0000 R CNN
F 1 "R" H 5130 1320 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 5130 1275 50  0001 C CNN
F 3 "~" H 5200 1275 50  0001 C CNN
	1    5200 1275
	0    1    1    0   
$EndComp
$Comp
L Device:R R21b1
U 1 1 5FF86EFA
P 5400 975
F 0 "R21b1" V 5607 975 50  0000 C CNN
F 1 "R" V 5516 975 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 5330 975 50  0001 C CNN
F 3 "~" H 5400 975 50  0001 C CNN
	1    5400 975 
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5FF86F00
P 5000 1725
F 0 "#PWR05" H 5000 1475 50  0001 C CNN
F 1 "GND" H 5005 1552 50  0000 C CNN
F 2 "" H 5000 1725 50  0001 C CNN
F 3 "" H 5000 1725 50  0001 C CNN
	1    5000 1725
	0    1    1    0   
$EndComp
Wire Wire Line
	5250 3275 5600 3275
Wire Wire Line
	5400 1425 5400 1275
Wire Wire Line
	5400 1275 5350 1275
Wire Wire Line
	5400 1275 5400 1125
Connection ~ 5400 1275
Wire Wire Line
	5000 1275 5050 1275
Wire Wire Line
	5000 1725 5200 1725
Connection ~ 5000 1725
Wire Wire Line
	5250 2425 5250 1975
Wire Wire Line
	5250 1975 5600 1975
Text GLabel 5000 2025 3    50   Input ~ 0
GND
Text GLabel 5150 3275 0    50   Input ~ 0
+24V
Wire Wire Line
	5150 3275 5250 3275
Connection ~ 5250 3275
Wire Wire Line
	5000 2025 5000 1725
Wire Wire Line
	5600 1975 5600 1725
Connection ~ 5600 1975
Wire Wire Line
	5400 825  5400 700 
Text HLabel 5400 700  1    50   Input ~ 0
SD2
Wire Wire Line
	6850 2825 6850 3250
Wire Wire Line
	6850 2525 6850 1950
Wire Wire Line
	6850 1950 7225 1950
$Comp
L dk_Transistors-Bipolar-BJT-Single:BC33725TA Q22
U 1 1 5FF86F27
P 7025 1600
F 0 "Q22" H 6925 1425 60  0000 L CNN
F 1 "BC33725TA" V 7275 1250 60  0000 L CNN
F 2 "digikey-footprints:TO-92-3_Formed_Leads" H 7225 1800 60  0001 L CNN
F 3 "https://www.onsemi.com/pub/Collateral/BC338-D.pdf" H 7225 1900 60  0001 L CNN
F 4 "BC33725TACT-ND" H 7225 2000 60  0001 L CNN "Digi-Key_PN"
F 5 "BC33725TA" H 7225 2100 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 7225 2200 60  0001 L CNN "Category"
F 7 "Transistors - Bipolar (BJT) - Single" H 7225 2300 60  0001 L CNN "Family"
F 8 "https://www.onsemi.com/pub/Collateral/BC338-D.pdf" H 7225 2400 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/on-semiconductor/BC33725TA/BC33725TACT-ND/1532787" H 7225 2500 60  0001 L CNN "DK_Detail_Page"
F 10 "TRANS NPN 45V 0.8A TO-92" H 7225 2600 60  0001 L CNN "Description"
F 11 "ON Semiconductor" H 7225 2700 60  0001 L CNN "Manufacturer"
F 12 "Active" H 7225 2800 60  0001 L CNN "Status"
	1    7025 1600
	0    1    1    0   
$EndComp
Wire Wire Line
	7225 1950 7225 1700
Connection ~ 7225 1950
Text GLabel 6750 3250 0    50   Input ~ 0
+24V
Wire Wire Line
	6750 3250 6850 3250
Connection ~ 6850 3250
Wire Wire Line
	6850 3250 7225 3250
Text GLabel 5700 3425 3    50   Input ~ 0
+24V
Text GLabel 5700 1775 1    50   Input ~ 0
GND
Wire Wire Line
	5800 1975 5800 1825
Wire Wire Line
	5900 3275 6175 3275
Wire Wire Line
	6175 3275 6175 1825
Wire Wire Line
	6175 1825 5800 1825
Wire Wire Line
	5900 1975 6300 1975
Wire Wire Line
	6300 1975 6300 3350
Wire Wire Line
	6300 3350 5800 3350
Connection ~ 5800 3350
Wire Wire Line
	5800 3350 5800 3275
$Comp
L Device:R R22e1
U 1 1 5FF86F3E
P 6775 1250
F 0 "R22e1" V 6675 1325 50  0000 R CNN
F 1 "10kΩ" V 6875 1350 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6705 1250 50  0001 C CNN
F 3 "~" H 6775 1250 50  0001 C CNN
	1    6775 1250
	0    1    1    0   
$EndComp
$Comp
L Device:R R22b1
U 1 1 5FF86F44
P 7025 975
F 0 "R22b1" V 7125 975 50  0000 C CNN
F 1 "10kΩ" V 6925 975 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6955 975 50  0001 C CNN
F 3 "~" H 7025 975 50  0001 C CNN
	1    7025 975 
	-1   0    0    1   
$EndComp
Wire Wire Line
	6925 1250 7025 1250
Wire Wire Line
	7025 1400 7025 1250
Connection ~ 7025 1250
Wire Wire Line
	7025 1250 7025 1125
Wire Wire Line
	7025 825  7025 700 
Text HLabel 7025 700  1    50   Input ~ 0
SM2
Wire Wire Line
	6825 1700 6550 1700
Wire Wire Line
	6550 1700 6550 1250
Wire Wire Line
	6550 1250 6625 1250
Text GLabel 6550 2000 3    50   Input ~ 0
GND
Wire Wire Line
	6550 2000 6550 1700
Connection ~ 6550 1700
$Comp
L power:GND #PWR09
U 1 1 5FF86F56
P 6550 1700
F 0 "#PWR09" H 6550 1450 50  0001 C CNN
F 1 "GND" H 6555 1527 50  0000 C CNN
F 2 "" H 6550 1700 50  0001 C CNN
F 3 "" H 6550 1700 50  0001 C CNN
	1    6550 1700
	0    1    1    0   
$EndComp
$Comp
L power:+24V #PWR08
U 1 1 5FF86F5C
P 5250 3275
F 0 "#PWR08" H 5250 3125 50  0001 C CNN
F 1 "+24V" V 5265 3403 50  0000 L CNN
F 2 "" H 5250 3275 50  0001 C CNN
F 3 "" H 5250 3275 50  0001 C CNN
	1    5250 3275
	-1   0    0    1   
$EndComp
$Comp
L power:+24V #PWR012
U 1 1 5FF86F62
P 6850 3250
F 0 "#PWR012" H 6850 3100 50  0001 C CNN
F 1 "+24V" V 6865 3378 50  0000 L CNN
F 2 "" H 6850 3250 50  0001 C CNN
F 3 "" H 6850 3250 50  0001 C CNN
	1    6850 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	7425 3425 8050 3425
Wire Wire Line
	8050 3425 8050 2975
Wire Wire Line
	7425 3425 7425 3250
Wire Wire Line
	8350 2975 8350 2650
Wire Wire Line
	8350 2650 8475 2650
Wire Wire Line
	8475 2550 8350 2550
Wire Wire Line
	8350 2550 8350 2300
Wire Wire Line
	5700 3425 5700 3275
Wire Wire Line
	5700 1975 5700 1775
Wire Wire Line
	7325 3600 7325 3250
Wire Wire Line
	7975 3650 7975 2300
Wire Wire Line
	7975 2300 8050 2300
$Comp
L CustomLib:HK19F-DC5V-SHGY_ K?
U 1 1 5FF92811
P 1350 6625
AR Path="/5FF92811" Ref="K?"  Part="1" 
AR Path="/5FF237A6/5FF92811" Ref="K31"  Part="1" 
F 0 "K31" V 2425 6850 50  0000 R CNN
F 1 "HK19F-DC5V-SHGY_" H 2425 6175 50  0000 R CNN
F 2 "CustomLib:HK19FDC5VSHGY" H 2500 6725 50  0001 L CNN
F 3 "https://datasheet.lcsc.com/szlcsc/Ningbo-Keke-New-Era-Appliance-HK19F-DC5V-SHG-Y_C54512.pdf" H 2500 6625 50  0001 L CNN
F 4 "Relays General Purpose Non Latching 5VDC DPDT 1A (125VAC , 24VDC) Max Double knives10*20 RoHS" H 2500 6525 50  0001 L CNN "Description"
F 5 "12.3" H 2500 6425 50  0001 L CNN "Height"
F 6 "HUIKE" H 2500 6325 50  0001 L CNN "Manufacturer_Name"
F 7 "HK19F-DC5V-SHG（Y)" H 2500 6225 50  0001 L CNN "Manufacturer_Part_Number"
	1    1350 6625
	0    -1   -1   0   
$EndComp
$Comp
L CustomLib:G5NB-1A-E-DC5 K?
U 1 1 5FF9281D
P 2975 6600
AR Path="/5FF9281D" Ref="K?"  Part="1" 
AR Path="/5FF237A6/5FF9281D" Ref="K32"  Part="1" 
F 0 "K32" V 4050 6725 50  0000 L CNN
F 1 "G5NB-1A-E-DC5" H 3300 6225 50  0000 L CNN
F 2 "CustomLib:G5NB1A4DC5" H 4125 6700 50  0001 L CNN
F 3 "https://omronfs.omron.com/en_US/ecb/products/pdf/en-g5nb.pdf" H 4125 6600 50  0001 L CNN
F 4 "General Purpose Relays SPST-NO Flux-Tight 5VDC High-Capacity" H 4125 6500 50  0001 L CNN "Description"
F 5 "15" H 4125 6400 50  0001 L CNN "Height"
F 6 "Omron Electronics" H 4125 6300 50  0001 L CNN "Manufacturer_Name"
F 7 "G5NB-1A-E-DC5" H 4125 6200 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "653-G5NB-1A-E-DC5" H 4125 5900 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Omron-Electronics/G5NB-1A-E-DC5?qs=3jhwTwe4bOEZ3SI7aFLd0Q%3D%3D" H 4125 5800 50  0001 L CNN "Mouser Price/Stock"
	1    2975 6600
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J?
U 1 1 5FF92823
P 4425 5900
AR Path="/5FF92823" Ref="J?"  Part="1" 
AR Path="/5FF237A6/5FF92823" Ref="J3"  Part="1" 
F 0 "J3" V 4297 5980 50  0000 L CNN
F 1 "Screw_Terminal_01x02" V 4388 5980 50  0000 L CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 4425 5900 50  0001 C CNN
F 3 "~" H 4425 5900 50  0001 C CNN
	1    4425 5900
	1    0    0    -1  
$EndComp
$Comp
L Device:Ferrite_Bead FB32
U 1 1 5FF92829
P 3950 6325
F 0 "FB32" H 4087 6371 50  0000 L CNN
F 1 "Ferrite_Bead" H 4087 6280 50  0000 L CNN
F 2 "Ferrite_THT:LairdTech_28C0236-0JW-10" V 3880 6325 50  0001 C CNN
F 3 "~" H 3950 6325 50  0001 C CNN
	1    3950 6325
	0    -1   -1   0   
$EndComp
$Comp
L Device:Ferrite_Bead FB31
U 1 1 5FF9282F
P 3950 5650
F 0 "FB31" H 4087 5696 50  0000 L CNN
F 1 "Ferrite_Bead" H 4087 5605 50  0000 L CNN
F 2 "Ferrite_THT:LairdTech_28C0236-0JW-10" V 3880 5650 50  0001 C CNN
F 3 "~" H 3950 5650 50  0001 C CNN
	1    3950 5650
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4007 D32
U 1 1 5FF92835
P 2600 6025
F 0 "D32" H 2550 6150 50  0000 L CNN
F 1 "1N4007" H 2475 5925 50  0000 L CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 2600 5850 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88503/1n4001.pdf" H 2600 6025 50  0001 C CNN
	1    2600 6025
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4007 D31
U 1 1 5FF9283B
P 1000 5925
F 0 "D31" H 950 6050 50  0000 L CNN
F 1 "1N4007" H 875 5825 50  0000 L CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 1000 5750 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88503/1n4001.pdf" H 1000 5925 50  0001 C CNN
	1    1000 5925
	0    -1   -1   0   
$EndComp
$Comp
L dk_Transistors-Bipolar-BJT-Single:BC33725TA Q31
U 1 1 5FF9284A
P 1150 4975
F 0 "Q31" H 1050 4800 60  0000 L CNN
F 1 "BC33725TA" V 1400 4625 60  0000 L CNN
F 2 "digikey-footprints:TO-92-3_Formed_Leads" H 1350 5175 60  0001 L CNN
F 3 "https://www.onsemi.com/pub/Collateral/BC338-D.pdf" H 1350 5275 60  0001 L CNN
F 4 "BC33725TACT-ND" H 1350 5375 60  0001 L CNN "Digi-Key_PN"
F 5 "BC33725TA" H 1350 5475 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 1350 5575 60  0001 L CNN "Category"
F 7 "Transistors - Bipolar (BJT) - Single" H 1350 5675 60  0001 L CNN "Family"
F 8 "https://www.onsemi.com/pub/Collateral/BC338-D.pdf" H 1350 5775 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/on-semiconductor/BC33725TA/BC33725TACT-ND/1532787" H 1350 5875 60  0001 L CNN "DK_Detail_Page"
F 10 "TRANS NPN 45V 0.8A TO-92" H 1350 5975 60  0001 L CNN "Description"
F 11 "ON Semiconductor" H 1350 6075 60  0001 L CNN "Manufacturer"
F 12 "Active" H 1350 6175 60  0001 L CNN "Status"
	1    1150 4975
	0    1    1    0   
$EndComp
$Comp
L Device:R R31e1
U 1 1 5FF92850
P 950 4625
F 0 "R31e1" H 880 4579 50  0000 R CNN
F 1 "R" H 880 4670 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 880 4625 50  0001 C CNN
F 3 "~" H 950 4625 50  0001 C CNN
	1    950  4625
	0    1    1    0   
$EndComp
$Comp
L Device:R R31b1
U 1 1 5FF92856
P 1150 4325
F 0 "R31b1" V 1357 4325 50  0000 C CNN
F 1 "R" V 1266 4325 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 1080 4325 50  0001 C CNN
F 3 "~" H 1150 4325 50  0001 C CNN
	1    1150 4325
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5FF9285C
P 750 5075
F 0 "#PWR01" H 750 4825 50  0001 C CNN
F 1 "GND" H 755 4902 50  0000 C CNN
F 2 "" H 750 5075 50  0001 C CNN
F 3 "" H 750 5075 50  0001 C CNN
	1    750  5075
	0    1    1    0   
$EndComp
Wire Wire Line
	1000 6625 1350 6625
Wire Wire Line
	1150 4775 1150 4625
Wire Wire Line
	1150 4625 1100 4625
Wire Wire Line
	1150 4625 1150 4475
Connection ~ 1150 4625
Wire Wire Line
	750  4625 800  4625
Wire Wire Line
	750  5075 950  5075
Connection ~ 750  5075
Wire Wire Line
	1000 5775 1000 5325
Wire Wire Line
	1000 5325 1350 5325
Text GLabel 750  5375 3    50   Input ~ 0
GND
Text GLabel 900  6625 0    50   Input ~ 0
+24V
Wire Wire Line
	900  6625 1000 6625
Connection ~ 1000 6625
Wire Wire Line
	750  5375 750  5075
Wire Wire Line
	1350 5325 1350 5075
Connection ~ 1350 5325
Wire Wire Line
	1150 4175 1150 4050
Text HLabel 1150 4050 1    50   Input ~ 0
SD3
Wire Wire Line
	2600 6175 2600 6600
Wire Wire Line
	2600 5875 2600 5300
Wire Wire Line
	2600 5300 2975 5300
$Comp
L dk_Transistors-Bipolar-BJT-Single:BC33725TA Q32
U 1 1 5FF92883
P 2775 4950
F 0 "Q32" H 2675 4775 60  0000 L CNN
F 1 "BC33725TA" V 3025 4600 60  0000 L CNN
F 2 "digikey-footprints:TO-92-3_Formed_Leads" H 2975 5150 60  0001 L CNN
F 3 "https://www.onsemi.com/pub/Collateral/BC338-D.pdf" H 2975 5250 60  0001 L CNN
F 4 "BC33725TACT-ND" H 2975 5350 60  0001 L CNN "Digi-Key_PN"
F 5 "BC33725TA" H 2975 5450 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 2975 5550 60  0001 L CNN "Category"
F 7 "Transistors - Bipolar (BJT) - Single" H 2975 5650 60  0001 L CNN "Family"
F 8 "https://www.onsemi.com/pub/Collateral/BC338-D.pdf" H 2975 5750 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/on-semiconductor/BC33725TA/BC33725TACT-ND/1532787" H 2975 5850 60  0001 L CNN "DK_Detail_Page"
F 10 "TRANS NPN 45V 0.8A TO-92" H 2975 5950 60  0001 L CNN "Description"
F 11 "ON Semiconductor" H 2975 6050 60  0001 L CNN "Manufacturer"
F 12 "Active" H 2975 6150 60  0001 L CNN "Status"
	1    2775 4950
	0    1    1    0   
$EndComp
Wire Wire Line
	2975 5300 2975 5050
Connection ~ 2975 5300
Text GLabel 2500 6600 0    50   Input ~ 0
+24V
Wire Wire Line
	2500 6600 2600 6600
Connection ~ 2600 6600
Wire Wire Line
	2600 6600 2975 6600
Text GLabel 1450 6775 3    50   Input ~ 0
+24V
Text GLabel 1450 5125 1    50   Input ~ 0
GND
Wire Wire Line
	1550 5325 1550 5175
Wire Wire Line
	1650 6625 1925 6625
Wire Wire Line
	1925 6625 1925 5175
Wire Wire Line
	1925 5175 1550 5175
Wire Wire Line
	1650 5325 2050 5325
Wire Wire Line
	2050 5325 2050 6700
Wire Wire Line
	2050 6700 1550 6700
Connection ~ 1550 6700
Wire Wire Line
	1550 6700 1550 6625
$Comp
L Device:R R32e1
U 1 1 5FF9289A
P 2525 4600
F 0 "R32e1" H 2455 4554 50  0000 R CNN
F 1 "R" H 2455 4645 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 2455 4600 50  0001 C CNN
F 3 "~" H 2525 4600 50  0001 C CNN
	1    2525 4600
	0    1    1    0   
$EndComp
$Comp
L Device:R R32b1
U 1 1 5FF928A0
P 2775 4325
F 0 "R32b1" V 2982 4325 50  0000 C CNN
F 1 "R" V 2891 4325 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 2705 4325 50  0001 C CNN
F 3 "~" H 2775 4325 50  0001 C CNN
	1    2775 4325
	-1   0    0    1   
$EndComp
Wire Wire Line
	2675 4600 2775 4600
Wire Wire Line
	2775 4750 2775 4600
Connection ~ 2775 4600
Wire Wire Line
	2775 4600 2775 4475
Wire Wire Line
	2775 4175 2775 4050
Text HLabel 2775 4050 1    50   Input ~ 0
SM3
Wire Wire Line
	2575 5050 2300 5050
Wire Wire Line
	2300 5050 2300 4600
Wire Wire Line
	2300 4600 2375 4600
Text GLabel 2300 5350 3    50   Input ~ 0
GND
Wire Wire Line
	2300 5350 2300 5050
Connection ~ 2300 5050
$Comp
L power:GND #PWR03
U 1 1 5FF928B2
P 2300 5050
F 0 "#PWR03" H 2300 4800 50  0001 C CNN
F 1 "GND" H 2305 4877 50  0000 C CNN
F 2 "" H 2300 5050 50  0001 C CNN
F 3 "" H 2300 5050 50  0001 C CNN
	1    2300 5050
	0    1    1    0   
$EndComp
$Comp
L power:+24V #PWR02
U 1 1 5FF928B8
P 1000 6625
F 0 "#PWR02" H 1000 6475 50  0001 C CNN
F 1 "+24V" V 1015 6753 50  0000 L CNN
F 2 "" H 1000 6625 50  0001 C CNN
F 3 "" H 1000 6625 50  0001 C CNN
	1    1000 6625
	-1   0    0    1   
$EndComp
$Comp
L power:+24V #PWR04
U 1 1 5FF928BE
P 2600 6600
F 0 "#PWR04" H 2600 6450 50  0001 C CNN
F 1 "+24V" V 2615 6728 50  0000 L CNN
F 2 "" H 2600 6600 50  0001 C CNN
F 3 "" H 2600 6600 50  0001 C CNN
	1    2600 6600
	-1   0    0    1   
$EndComp
Wire Wire Line
	3175 6775 3800 6775
Wire Wire Line
	3800 6775 3800 6325
Wire Wire Line
	3175 6775 3175 6600
Wire Wire Line
	4100 6325 4100 6000
Wire Wire Line
	4100 6000 4225 6000
Wire Wire Line
	4225 5900 4100 5900
Wire Wire Line
	4100 5900 4100 5650
Wire Wire Line
	1450 6775 1450 6625
Wire Wire Line
	1450 5325 1450 5125
Wire Wire Line
	3075 6950 3075 6600
Wire Wire Line
	3725 7000 3725 5650
Wire Wire Line
	3725 5650 3800 5650
$Comp
L CustomLib:HK19F-DC5V-SHGY_ K?
U 1 1 60047C22
P 5150 4600
AR Path="/60047C22" Ref="K?"  Part="1" 
AR Path="/5FF237A6/60047C22" Ref="K41"  Part="1" 
F 0 "K41" V 6225 4825 50  0000 R CNN
F 1 "HK19F-DC5V-SHGY_" H 6225 4150 50  0000 R CNN
F 2 "CustomLib:HK19FDC5VSHGY" H 6300 4700 50  0001 L CNN
F 3 "https://datasheet.lcsc.com/szlcsc/Ningbo-Keke-New-Era-Appliance-HK19F-DC5V-SHG-Y_C54512.pdf" H 6300 4600 50  0001 L CNN
F 4 "Relays General Purpose Non Latching 5VDC DPDT 1A (125VAC , 24VDC) Max Double knives10*20 RoHS" H 6300 4500 50  0001 L CNN "Description"
F 5 "12.3" H 6300 4400 50  0001 L CNN "Height"
F 6 "HUIKE" H 6300 4300 50  0001 L CNN "Manufacturer_Name"
F 7 "HK19F-DC5V-SHG（Y)" H 6300 4200 50  0001 L CNN "Manufacturer_Part_Number"
	1    5150 4600
	1    0    0    -1  
$EndComp
$Comp
L CustomLib:G5NB-1A-E-DC5 K?
U 1 1 60047C2E
P 5175 6225
AR Path="/60047C2E" Ref="K?"  Part="1" 
AR Path="/5FF237A6/60047C2E" Ref="K42"  Part="1" 
F 0 "K42" V 6250 6350 50  0000 L CNN
F 1 "G5NB-1A-E-DC5" H 5500 5850 50  0000 L CNN
F 2 "CustomLib:G5NB1A4DC5" H 6325 6325 50  0001 L CNN
F 3 "https://omronfs.omron.com/en_US/ecb/products/pdf/en-g5nb.pdf" H 6325 6225 50  0001 L CNN
F 4 "General Purpose Relays SPST-NO Flux-Tight 5VDC High-Capacity" H 6325 6125 50  0001 L CNN "Description"
F 5 "15" H 6325 6025 50  0001 L CNN "Height"
F 6 "Omron Electronics" H 6325 5925 50  0001 L CNN "Manufacturer_Name"
F 7 "G5NB-1A-E-DC5" H 6325 5825 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "653-G5NB-1A-E-DC5" H 6325 5525 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Omron-Electronics/G5NB-1A-E-DC5?qs=3jhwTwe4bOEZ3SI7aFLd0Q%3D%3D" H 6325 5425 50  0001 L CNN "Mouser Price/Stock"
	1    5175 6225
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J?
U 1 1 60047C34
P 5875 7675
AR Path="/60047C34" Ref="J?"  Part="1" 
AR Path="/5FF237A6/60047C34" Ref="J4"  Part="1" 
F 0 "J4" V 5747 7755 50  0000 L CNN
F 1 "Screw_Terminal_01x02" V 5838 7755 50  0000 L CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 5875 7675 50  0001 C CNN
F 3 "~" H 5875 7675 50  0001 C CNN
	1    5875 7675
	0    1    1    0   
$EndComp
$Comp
L Device:Ferrite_Bead FB42
U 1 1 60047C3A
P 5450 7200
F 0 "FB42" H 5587 7246 50  0000 L CNN
F 1 "Ferrite_Bead" H 5587 7155 50  0000 L CNN
F 2 "Ferrite_THT:LairdTech_28C0236-0JW-10" V 5380 7200 50  0001 C CNN
F 3 "~" H 5450 7200 50  0001 C CNN
	1    5450 7200
	1    0    0    -1  
$EndComp
$Comp
L Device:Ferrite_Bead FB41
U 1 1 60047C40
P 6125 7200
F 0 "FB41" H 6262 7246 50  0000 L CNN
F 1 "Ferrite_Bead" H 6262 7155 50  0000 L CNN
F 2 "Ferrite_THT:LairdTech_28C0236-0JW-10" V 6055 7200 50  0001 C CNN
F 3 "~" H 6125 7200 50  0001 C CNN
	1    6125 7200
	1    0    0    -1  
$EndComp
$Comp
L Diode:1N4007 D42
U 1 1 60047C46
P 5750 5850
F 0 "D42" H 5700 5975 50  0000 L CNN
F 1 "1N4007" H 5625 5750 50  0000 L CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 5750 5675 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88503/1n4001.pdf" H 5750 5850 50  0001 C CNN
	1    5750 5850
	1    0    0    -1  
$EndComp
$Comp
L Diode:1N4007 D41
U 1 1 60047C4C
P 5850 4250
F 0 "D41" H 5800 4375 50  0000 L CNN
F 1 "1N4007" H 5725 4150 50  0000 L CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 5850 4075 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88503/1n4001.pdf" H 5850 4250 50  0001 C CNN
	1    5850 4250
	1    0    0    -1  
$EndComp
$Comp
L dk_Transistors-Bipolar-BJT-Single:BC33725TA Q41
U 1 1 60047C5B
P 6800 4400
F 0 "Q41" H 6700 4225 60  0000 L CNN
F 1 "BC33725TA" V 7050 4050 60  0000 L CNN
F 2 "digikey-footprints:TO-92-3_Formed_Leads" H 7000 4600 60  0001 L CNN
F 3 "https://www.onsemi.com/pub/Collateral/BC338-D.pdf" H 7000 4700 60  0001 L CNN
F 4 "BC33725TACT-ND" H 7000 4800 60  0001 L CNN "Digi-Key_PN"
F 5 "BC33725TA" H 7000 4900 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 7000 5000 60  0001 L CNN "Category"
F 7 "Transistors - Bipolar (BJT) - Single" H 7000 5100 60  0001 L CNN "Family"
F 8 "https://www.onsemi.com/pub/Collateral/BC338-D.pdf" H 7000 5200 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/on-semiconductor/BC33725TA/BC33725TACT-ND/1532787" H 7000 5300 60  0001 L CNN "DK_Detail_Page"
F 10 "TRANS NPN 45V 0.8A TO-92" H 7000 5400 60  0001 L CNN "Description"
F 11 "ON Semiconductor" H 7000 5500 60  0001 L CNN "Manufacturer"
F 12 "Active" H 7000 5600 60  0001 L CNN "Status"
	1    6800 4400
	-1   0    0    1   
$EndComp
$Comp
L Device:R R41e1
U 1 1 60047C61
P 7150 4200
F 0 "R41e1" H 7080 4154 50  0000 R CNN
F 1 "R" H 7080 4245 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7080 4200 50  0001 C CNN
F 3 "~" H 7150 4200 50  0001 C CNN
	1    7150 4200
	-1   0    0    1   
$EndComp
$Comp
L Device:R R41b1
U 1 1 60047C67
P 7450 4400
F 0 "R41b1" V 7657 4400 50  0000 C CNN
F 1 "R" V 7566 4400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7380 4400 50  0001 C CNN
F 3 "~" H 7450 4400 50  0001 C CNN
	1    7450 4400
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR010
U 1 1 60047C6D
P 6700 4000
F 0 "#PWR010" H 6700 3750 50  0001 C CNN
F 1 "GND" H 6705 3827 50  0000 C CNN
F 2 "" H 6700 4000 50  0001 C CNN
F 3 "" H 6700 4000 50  0001 C CNN
	1    6700 4000
	-1   0    0    1   
$EndComp
Wire Wire Line
	5150 4250 5150 4600
Wire Wire Line
	7000 4400 7150 4400
Wire Wire Line
	7150 4400 7150 4350
Wire Wire Line
	7150 4400 7300 4400
Connection ~ 7150 4400
Wire Wire Line
	7150 4000 7150 4050
Wire Wire Line
	6700 4000 6700 4200
Connection ~ 6700 4000
Wire Wire Line
	6000 4250 6450 4250
Wire Wire Line
	6450 4250 6450 4600
Text GLabel 6400 4000 0    50   Input ~ 0
GND
Text GLabel 5150 4150 1    50   Input ~ 0
+24V
Wire Wire Line
	5150 4150 5150 4250
Connection ~ 5150 4250
Wire Wire Line
	6400 4000 6700 4000
Wire Wire Line
	6450 4600 6700 4600
Connection ~ 6450 4600
Wire Wire Line
	7600 4400 7725 4400
Text HLabel 7725 4400 2    50   Input ~ 0
SD4
Wire Wire Line
	5600 5850 5175 5850
Wire Wire Line
	5900 5850 6475 5850
Wire Wire Line
	6475 5850 6475 6225
$Comp
L dk_Transistors-Bipolar-BJT-Single:BC33725TA Q42
U 1 1 60047C94
P 6825 6025
F 0 "Q42" H 6725 5850 60  0000 L CNN
F 1 "BC33725TA" V 7075 5675 60  0000 L CNN
F 2 "digikey-footprints:TO-92-3_Formed_Leads" H 7025 6225 60  0001 L CNN
F 3 "https://www.onsemi.com/pub/Collateral/BC338-D.pdf" H 7025 6325 60  0001 L CNN
F 4 "BC33725TACT-ND" H 7025 6425 60  0001 L CNN "Digi-Key_PN"
F 5 "BC33725TA" H 7025 6525 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 7025 6625 60  0001 L CNN "Category"
F 7 "Transistors - Bipolar (BJT) - Single" H 7025 6725 60  0001 L CNN "Family"
F 8 "https://www.onsemi.com/pub/Collateral/BC338-D.pdf" H 7025 6825 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/on-semiconductor/BC33725TA/BC33725TACT-ND/1532787" H 7025 6925 60  0001 L CNN "DK_Detail_Page"
F 10 "TRANS NPN 45V 0.8A TO-92" H 7025 7025 60  0001 L CNN "Description"
F 11 "ON Semiconductor" H 7025 7125 60  0001 L CNN "Manufacturer"
F 12 "Active" H 7025 7225 60  0001 L CNN "Status"
	1    6825 6025
	-1   0    0    1   
$EndComp
Wire Wire Line
	6475 6225 6725 6225
Connection ~ 6475 6225
Text GLabel 5175 5750 1    50   Input ~ 0
+24V
Wire Wire Line
	5175 5750 5175 5850
Connection ~ 5175 5850
Wire Wire Line
	5175 5850 5175 6225
Text GLabel 5000 4700 0    50   Input ~ 0
+24V
Text GLabel 6650 4700 2    50   Input ~ 0
GND
Wire Wire Line
	6450 4800 6600 4800
Wire Wire Line
	5150 4900 5150 5175
Wire Wire Line
	5150 5175 6600 5175
Wire Wire Line
	6600 5175 6600 4800
Wire Wire Line
	6450 4900 6450 5300
Wire Wire Line
	6450 5300 5075 5300
Wire Wire Line
	5075 5300 5075 4800
Connection ~ 5075 4800
Wire Wire Line
	5075 4800 5150 4800
$Comp
L Device:R R42e1
U 1 1 60047CAB
P 7175 5775
F 0 "R42e1" H 7105 5729 50  0000 R CNN
F 1 "R" H 7105 5820 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7105 5775 50  0001 C CNN
F 3 "~" H 7175 5775 50  0001 C CNN
	1    7175 5775
	-1   0    0    1   
$EndComp
$Comp
L Device:R R42b1
U 1 1 60047CB1
P 7450 6025
F 0 "R42b1" V 7657 6025 50  0000 C CNN
F 1 "R" V 7566 6025 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7380 6025 50  0001 C CNN
F 3 "~" H 7450 6025 50  0001 C CNN
	1    7450 6025
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7175 5925 7175 6025
Wire Wire Line
	7025 6025 7175 6025
Connection ~ 7175 6025
Wire Wire Line
	7175 6025 7300 6025
Wire Wire Line
	7600 6025 7725 6025
Text HLabel 7725 6025 2    50   Input ~ 0
SM4
Wire Wire Line
	6725 5825 6725 5550
Wire Wire Line
	6725 5550 7175 5550
Wire Wire Line
	7175 5550 7175 5625
Text GLabel 6425 5550 0    50   Input ~ 0
GND
Wire Wire Line
	6425 5550 6725 5550
Connection ~ 6725 5550
$Comp
L power:GND #PWR011
U 1 1 60047CC3
P 6725 5550
F 0 "#PWR011" H 6725 5300 50  0001 C CNN
F 1 "GND" H 6730 5377 50  0000 C CNN
F 2 "" H 6725 5550 50  0001 C CNN
F 3 "" H 6725 5550 50  0001 C CNN
	1    6725 5550
	-1   0    0    1   
$EndComp
$Comp
L power:+24V #PWR06
U 1 1 60047CC9
P 5150 4250
F 0 "#PWR06" H 5150 4100 50  0001 C CNN
F 1 "+24V" V 5165 4378 50  0000 L CNN
F 2 "" H 5150 4250 50  0001 C CNN
F 3 "" H 5150 4250 50  0001 C CNN
	1    5150 4250
	0    -1   -1   0   
$EndComp
$Comp
L power:+24V #PWR07
U 1 1 60047CCF
P 5175 5850
F 0 "#PWR07" H 5175 5700 50  0001 C CNN
F 1 "+24V" V 5190 5978 50  0000 L CNN
F 2 "" H 5175 5850 50  0001 C CNN
F 3 "" H 5175 5850 50  0001 C CNN
	1    5175 5850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5000 6425 5000 7050
Wire Wire Line
	5000 7050 5450 7050
Wire Wire Line
	5000 6425 5175 6425
Wire Wire Line
	5450 7350 5775 7350
Wire Wire Line
	5775 7350 5775 7475
Wire Wire Line
	5875 7475 5875 7350
Wire Wire Line
	5875 7350 6125 7350
Wire Wire Line
	5000 4700 5150 4700
Wire Wire Line
	6450 4700 6650 4700
Wire Wire Line
	4825 6325 5175 6325
Wire Wire Line
	4775 6975 6125 6975
Wire Wire Line
	6125 6975 6125 7050
Wire Wire Line
	750  4625 750  5075
Wire Wire Line
	1025 2725 1025 3275
Wire Wire Line
	5000 1275 5000 1725
Wire Wire Line
	5150 4250 5700 4250
Wire Wire Line
	6700 4000 7150 4000
Text GLabel 9525 1025 0    50   Input ~ 0
GND
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 6006BA9B
P 9900 1025
F 0 "#FLG0101" H 9900 1100 50  0001 C CNN
F 1 "PWR_FLAG" V 9900 1153 50  0000 L CNN
F 2 "" H 9900 1025 50  0001 C CNN
F 3 "~" H 9900 1025 50  0001 C CNN
	1    9900 1025
	0    1    1    0   
$EndComp
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 6006CC46
P 9900 1300
F 0 "#FLG0102" H 9900 1375 50  0001 C CNN
F 1 "PWR_FLAG" V 9900 1428 50  0000 L CNN
F 2 "" H 9900 1300 50  0001 C CNN
F 3 "~" H 9900 1300 50  0001 C CNN
	1    9900 1300
	0    1    1    0   
$EndComp
Text GLabel 9525 1300 0    50   Input ~ 0
+24V
Wire Wire Line
	9525 1300 9900 1300
Wire Wire Line
	9525 1025 9900 1025
Wire Wire Line
	775  1275 775  1725
Wire Wire Line
	1000 6075 1000 6625
Wire Wire Line
	5250 2725 5250 3275
Text Label 1550 7000 0    50   ~ 0
SPWR_3b
Text Label 1625 5175 0    50   ~ 0
SPWR_3a
Text Label 3325 6775 0    50   ~ 0
SPWR_3c
Text Label 7600 3425 0    50   ~ 0
SPWR_2c
Text Label 5000 6525 3    50   ~ 0
SPWR_4c
Text Label 1575 3650 0    50   ~ 0
SPWR_1b
Text Label 1650 1825 0    50   ~ 0
SPWR_1a
Text Label 5875 1825 0    50   ~ 0
SPWR_2a
Text Label 5800 3650 0    50   ~ 0
SPWR_2b
Text Label 4775 4800 3    50   ~ 0
SPWR_4b
Text Label 6600 4875 3    50   ~ 0
SPWR_4a
Text Label 4100 5650 0    50   ~ 0
SPWR_3d
Text Label 4100 6325 0    50   ~ 0
SPWR_3e
Text Label 4125 2300 0    50   ~ 0
SPWR_1d
Text Label 4125 2975 0    50   ~ 0
SPWR_1e
Text Label 8350 2300 0    50   ~ 0
SPWR_2d
Text Label 8350 2975 0    50   ~ 0
SPWR_2e
Text Label 6125 7350 0    50   ~ 0
SPWR_4d
Text Label 5450 7350 2    50   ~ 0
SPWR_4e
Wire Wire Line
	1950 3275 1950 3600
Wire Wire Line
	1950 3600 3100 3600
Connection ~ 1950 3275
Wire Wire Line
	1575 3650 3750 3650
Wire Wire Line
	1575 3350 1575 3650
Wire Wire Line
	7975 3650 5800 3650
Wire Wire Line
	5800 3350 5800 3650
Wire Wire Line
	7325 3600 6175 3600
Wire Wire Line
	6175 3600 6175 3275
Connection ~ 6175 3275
Wire Wire Line
	4775 4800 4775 6975
Wire Wire Line
	4775 4800 5075 4800
Wire Wire Line
	5150 5175 4825 5175
Wire Wire Line
	4825 5175 4825 6325
Connection ~ 5150 5175
Wire Wire Line
	3725 7000 1550 7000
Wire Wire Line
	1550 6700 1550 7000
Wire Wire Line
	1925 6625 1925 6950
Wire Wire Line
	1925 6950 3075 6950
Connection ~ 1925 6625
$Comp
L Device:LED_Dual_2pin D1
U 1 1 5FF5037D
P 3600 1800
F 0 "D1" V 3554 2058 50  0000 L CNN
F 1 "LED_Dual_2pin" V 3900 1850 50  0000 L CNN
F 2 "LED_THT:LED_D5.0mm" H 3600 1800 50  0001 C CNN
F 3 "~" H 3600 1800 50  0001 C CNN
	1    3600 1800
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R1D1
U 1 1 5FF524FE
P 3600 2450
F 0 "R1D1" H 3530 2404 50  0000 R CNN
F 1 "R" H 3530 2495 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3530 2450 50  0001 C CNN
F 3 "~" H 3600 2450 50  0001 C CNN
	1    3600 2450
	-1   0    0    1   
$EndComp
Wire Wire Line
	3600 2300 3600 2100
$Comp
L Device:LED_Dual_2pin D2
U 1 1 5FFAF0D6
P 7825 1800
F 0 "D2" V 7779 2058 50  0000 L CNN
F 1 "LED_Dual_2pin" V 8150 1875 50  0000 L CNN
F 2 "LED_THT:LED_D5.0mm" H 7825 1800 50  0001 C CNN
F 3 "~" H 7825 1800 50  0001 C CNN
	1    7825 1800
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R2D1
U 1 1 5FFAF0DC
P 7825 2450
F 0 "R2D1" H 7755 2404 50  0000 R CNN
F 1 "R" H 7755 2495 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7755 2450 50  0001 C CNN
F 3 "~" H 7825 2450 50  0001 C CNN
	1    7825 2450
	-1   0    0    1   
$EndComp
Wire Wire Line
	7825 2300 7825 2100
$Comp
L Device:LED_Dual_2pin D3
U 1 1 5FFC9436
P 3600 5175
F 0 "D3" V 3554 5433 50  0000 L CNN
F 1 "LED_Dual_2pin" V 3900 5225 50  0000 L CNN
F 2 "LED_THT:LED_D5.0mm" H 3600 5175 50  0001 C CNN
F 3 "~" H 3600 5175 50  0001 C CNN
	1    3600 5175
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R3D1
U 1 1 5FFC943C
P 3600 5825
F 0 "R3D1" H 3530 5779 50  0000 R CNN
F 1 "R" H 3530 5870 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3530 5825 50  0001 C CNN
F 3 "~" H 3600 5825 50  0001 C CNN
	1    3600 5825
	-1   0    0    1   
$EndComp
Wire Wire Line
	3600 5675 3600 5475
$Comp
L Device:LED_Dual_2pin D4
U 1 1 5FFD729B
P 6475 6800
F 0 "D4" V 6429 7058 50  0000 L CNN
F 1 "LED_Dual_2pin" V 6775 6850 50  0000 L CNN
F 2 "LED_THT:LED_D5.0mm" H 6475 6800 50  0001 C CNN
F 3 "~" H 6475 6800 50  0001 C CNN
	1    6475 6800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4D1
U 1 1 5FFD72A1
P 5825 6800
F 0 "R4D1" V 6032 6800 50  0000 C CNN
F 1 "R" V 5941 6800 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 5755 6800 50  0001 C CNN
F 3 "~" H 5825 6800 50  0001 C CNN
	1    5825 6800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5975 6800 6175 6800
Text Label 3325 3425 0    50   ~ 0
SPWR_1c
Wire Wire Line
	3200 3425 3825 3425
Wire Wire Line
	3600 1500 3825 1500
Wire Wire Line
	3825 1500 3825 2300
Connection ~ 3825 2300
Wire Wire Line
	3600 2600 3600 2975
Wire Wire Line
	3600 2975 3825 2975
Connection ~ 3825 2975
Wire Wire Line
	7825 2600 7825 2975
Wire Wire Line
	7825 2975 8050 2975
Connection ~ 8050 2975
Wire Wire Line
	7825 1500 8050 1500
Wire Wire Line
	8050 1500 8050 2300
Connection ~ 8050 2300
Wire Wire Line
	3600 5975 3600 6325
Wire Wire Line
	3600 6325 3800 6325
Connection ~ 3800 6325
Wire Wire Line
	3600 4875 3800 4875
Wire Wire Line
	3800 4875 3800 5650
Connection ~ 3800 5650
Connection ~ 5450 7050
Wire Wire Line
	5450 6800 5675 6800
Wire Wire Line
	5450 6800 5450 7050
Wire Wire Line
	6775 6800 6775 7050
Wire Wire Line
	6775 7050 6125 7050
Connection ~ 6125 7050
$EndSCHEMATC
