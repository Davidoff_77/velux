EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 2
Title "Velux Shutter Control"
Date "2021-01-03"
Rev "1"
Comp "David Hruby"
Comment1 "Arduino Control"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 7425 950  950  1450
U 5FF237A6
F0 "Relays" 50
F1 "Relays.sch" 50
F2 "SD1" I L 7425 1050 50 
F3 "SM1" I L 7425 1175 50 
F4 "SD2" I L 7425 1350 50 
F5 "SM2" I L 7425 1475 50 
F6 "SD3" I R 8375 1050 50 
F7 "SM3" I R 8375 1175 50 
F8 "SD4" I R 8375 1350 50 
F9 "SM4" I R 8375 1475 50 
$EndSheet
$Comp
L Regulator_Switching:TSR_1-2433 U1
U 1 1 5FF314B7
P 2125 2250
F 0 "U1" H 2125 2617 50  0000 C CNN
F 1 "TSR_1-2433" H 2125 2526 50  0000 C CNN
F 2 "Converter_DCDC:Converter_DCDC_TRACO_TSR-1_THT" H 2125 2100 50  0001 L CIN
F 3 "http://www.tracopower.com/products/tsr1.pdf" H 2125 2250 50  0001 C CNN
	1    2125 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 1050 7425 1050
Text GLabel 2125 3050 3    50   Input ~ 0
GND
Text GLabel 1550 2150 0    50   Input ~ 0
+24V
Wire Wire Line
	1550 2150 1725 2150
Wire Wire Line
	3475 2875 2975 2875
Wire Wire Line
	2975 2150 2525 2150
NoConn ~ 6075 2425
NoConn ~ 6075 2525
NoConn ~ 6075 2625
NoConn ~ 6075 2925
NoConn ~ 3475 2975
NoConn ~ 3475 2175
NoConn ~ 3475 2075
NoConn ~ 3475 1975
NoConn ~ 3475 1875
NoConn ~ 3475 1775
NoConn ~ 3475 1675
$Comp
L CustomLib:USR-ES1 IC1
U 1 1 5FF7588D
P 2975 4350
F 0 "IC1" H 3575 4615 50  0000 C CNN
F 1 "USR-ES1" H 3575 4524 50  0000 C CNN
F 2 "CustomLib:USRES1" H 4025 4450 50  0001 L CNN
F 3 "https://www.aliexpress.com/item/Free-Shipping-USR-ES1-W5500-Chip-New-SPI-to-LAN-Ethernet-Converter-TCP-IP-Mod/32714438223.html?spm=a2g0s.9042311.0.0.27424c4dBBEz2G" H 4025 4350 50  0001 L CNN
F 4 "USR-ES1 W5500 Chip New SPI to LAN/ Ethernet Converter TCP/IP Mod" H 4025 4250 50  0001 L CNN "Description"
F 5 "3" H 4025 4150 50  0001 L CNN "Height"
F 6 "Smart Electronics" H 4025 4050 50  0001 L CNN "Manufacturer_Name"
F 7 "USR-ES1" H 4025 3950 50  0001 L CNN "Manufacturer_Part_Number"
	1    2975 4350
	1    0    0    -1  
$EndComp
$Comp
L arduino:Arduino_Mini_3_3_Socket XA1
U 1 1 6005BF37
P 4775 2175
F 0 "XA1" H 4775 3462 60  0000 C CNN
F 1 "Arduino_Mini_3_3_Socket" H 4775 3356 60  0000 C CNN
F 2 "Arduino:Arduino_Mini_3_3_Socket" H 6575 5925 60  0001 C CNN
F 3 "https://store.arduino.cc/arduino-mini-05" H 6575 5925 60  0001 C CNN
	1    4775 2175
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 4350 2975 4350
Wire Wire Line
	4550 4450 4550 3325
Wire Wire Line
	4550 3325 2975 3325
Wire Wire Line
	2975 3325 2975 2875
Connection ~ 2975 2875
Wire Wire Line
	2975 2150 2975 2875
Wire Wire Line
	2975 4450 2975 4350
Connection ~ 2975 4350
Wire Wire Line
	4175 4550 4175 4450
Connection ~ 4175 4450
Wire Wire Line
	4175 4450 4550 4450
NoConn ~ 4175 4350
NoConn ~ 6075 1275
$Comp
L Connector:Screw_Terminal_01x02 J5
U 1 1 5FFA2988
P 1175 825
AR Path="/5FFA2988" Ref="J5"  Part="1" 
AR Path="/5FF237A6/5FFA2988" Ref="J?"  Part="1" 
F 0 "J5" V 1047 905 50  0000 L CNN
F 1 "PWR 24V" V 1138 905 50  0000 L CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 1175 825 50  0001 C CNN
F 3 "~" H 1175 825 50  0001 C CNN
	1    1175 825 
	0    -1   -1   0   
$EndComp
Text GLabel 1275 1300 3    50   Input ~ 0
+24V
Text GLabel 1175 1350 3    50   Input ~ 0
GND
$Comp
L Device:LED D5
U 1 1 600B4CFD
P 1900 1050
F 0 "D5" H 1900 850 50  0000 C CNN
F 1 "LED" H 1900 925 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm" H 1900 1050 50  0001 C CNN
F 3 "~" H 1900 1050 50  0001 C CNN
	1    1900 1050
	-1   0    0    1   
$EndComp
$Comp
L Device:R R5
U 1 1 600B5591
P 1500 1050
F 0 "R5" V 1650 1050 50  0000 C CNN
F 1 "R" V 1575 1050 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 1430 1050 50  0001 C CNN
F 3 "~" H 1500 1050 50  0001 C CNN
	1    1500 1050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1175 1025 1175 1250
Wire Wire Line
	1275 1025 1275 1050
Wire Wire Line
	1275 1050 1350 1050
Connection ~ 1275 1050
Wire Wire Line
	1275 1050 1275 1300
Wire Wire Line
	1650 1050 1750 1050
Wire Wire Line
	2050 1050 2050 1250
Wire Wire Line
	2050 1250 1175 1250
Connection ~ 1175 1250
Wire Wire Line
	1175 1250 1175 1350
Wire Wire Line
	8375 1050 8750 1050
Wire Wire Line
	8375 1175 8750 1175
Text Label 8450 1050 0    50   ~ 0
SD3b
Text Label 8450 1175 0    50   ~ 0
SM3b
Wire Wire Line
	7250 1175 7425 1175
Text Label 7250 1050 0    50   ~ 0
SD1b
Text Label 7250 1175 0    50   ~ 0
SM1b
Entry Wire Line
	7150 950  7250 1050
Entry Wire Line
	7150 1075 7250 1175
Entry Wire Line
	7150 1250 7250 1350
Entry Wire Line
	7150 1375 7250 1475
Wire Wire Line
	7250 1350 7425 1350
Wire Wire Line
	7250 1475 7425 1475
Text Label 7250 1350 0    50   ~ 0
SD2b
Text Label 7250 1475 0    50   ~ 0
SM2b
Entry Wire Line
	3175 2175 3275 2275
Entry Wire Line
	3175 2275 3275 2375
Wire Wire Line
	3275 2275 3475 2275
Wire Wire Line
	3275 2375 3475 2375
Text Label 3275 2375 0    50   ~ 0
SM4b
Text Label 3275 2275 0    50   ~ 0
SD4b
Entry Wire Line
	8750 1050 8850 1150
Entry Wire Line
	8750 1175 8850 1275
Entry Wire Line
	8750 1350 8850 1450
Entry Wire Line
	8750 1475 8850 1575
Wire Wire Line
	8375 1475 8750 1475
Wire Wire Line
	8750 1350 8375 1350
Text Label 8450 1350 0    50   ~ 0
SD4b
Text Label 8450 1475 0    50   ~ 0
SM4b
Wire Wire Line
	6075 1175 6675 1175
Wire Wire Line
	6075 2075 6675 2075
Wire Wire Line
	6075 2175 6675 2175
Wire Wire Line
	6075 2275 6675 2275
Entry Wire Line
	6675 1175 6775 1275
Entry Wire Line
	6675 1975 6775 2075
Entry Wire Line
	6675 2075 6775 2175
Entry Wire Line
	6675 2175 6775 2275
Entry Wire Line
	6675 2275 6775 2375
Text Label 6675 1175 0    50   ~ 0
INTb
Text Label 6375 1975 0    50   ~ 0
CSb
Text Label 6375 2275 0    50   ~ 0
SCLKb
Text Label 6375 2175 0    50   ~ 0
MISOb
Text Label 6375 2075 0    50   ~ 0
MOSIb
Connection ~ 6275 675 
Wire Bus Line
	3175 675  6275 675 
Wire Bus Line
	6275 675  7150 675 
Text Label 6075 1375 0    50   ~ 0
SM1b
Text Label 6075 1475 0    50   ~ 0
SD1b
Wire Wire Line
	6075 1875 6175 1875
Entry Wire Line
	6175 1875 6275 1975
Wire Wire Line
	6075 1775 6175 1775
Text Label 6075 1575 0    50   ~ 0
SM2b
Text Label 6075 1675 0    50   ~ 0
SD2b
Wire Wire Line
	6075 1675 6175 1675
Wire Wire Line
	6075 1575 6175 1575
Text Label 6075 1875 0    50   ~ 0
SD3b
Text Label 6075 1775 0    50   ~ 0
SM3b
Wire Wire Line
	6075 1475 6175 1475
Wire Wire Line
	6075 1375 6175 1375
Entry Wire Line
	6175 1775 6275 1875
Entry Wire Line
	6175 1675 6275 1775
Entry Wire Line
	6175 1575 6275 1675
Entry Wire Line
	6175 1375 6275 1475
Entry Wire Line
	6175 1475 6275 1575
Entry Wire Line
	4300 4750 4400 4850
Entry Wire Line
	4300 4850 4400 4950
Entry Wire Line
	2575 4450 2675 4550
Entry Wire Line
	2575 4550 2675 4650
Entry Wire Line
	2575 4650 2675 4750
Entry Wire Line
	2575 4750 2675 4850
Wire Wire Line
	4175 4750 4300 4750
Wire Wire Line
	4175 4850 4300 4850
Wire Wire Line
	2675 4550 2975 4550
Wire Wire Line
	2675 4650 2975 4650
Wire Wire Line
	2675 4750 2975 4750
Wire Wire Line
	2675 4850 2975 4850
Wire Bus Line
	2575 5050 4400 5050
Wire Bus Line
	4400 5050 6775 5050
Connection ~ 4400 5050
Text Label 4300 4750 0    50   ~ 0
RSTb
Text Label 4300 4850 0    50   ~ 0
MISOb
Text Label 2675 4550 0    50   ~ 0
MOSIb
Text Label 2675 4650 0    50   ~ 0
SCLKb
Text Label 2675 4750 0    50   ~ 0
CSb
Text Label 2675 4850 0    50   ~ 0
INTb
Entry Wire Line
	6675 3075 6775 3175
Wire Wire Line
	6075 3075 6675 3075
Text Label 6375 3075 0    50   ~ 0
RSTb
Wire Bus Line
	7150 675  8850 675 
Connection ~ 7150 675 
Wire Bus Line
	7150 675  8500 675 
Wire Wire Line
	2125 2775 2750 2775
Wire Wire Line
	2125 2450 2125 2775
Connection ~ 2750 2775
Wire Wire Line
	2750 2775 3375 2775
Wire Wire Line
	2125 3050 2125 2775
Connection ~ 2125 2775
Wire Wire Line
	2750 2775 2750 4350
NoConn ~ 6075 2725
NoConn ~ 6075 2825
NoConn ~ 3475 1275
NoConn ~ 3475 1375
Wire Wire Line
	3475 2575 3375 2575
Wire Wire Line
	3375 2575 3375 2675
Connection ~ 3375 2775
Wire Wire Line
	3375 2775 3475 2775
Wire Wire Line
	3475 2675 3375 2675
Wire Wire Line
	6075 1975 6675 1975
Wire Bus Line
	4400 4850 4400 5050
Wire Bus Line
	3175 675  3175 2275
Wire Bus Line
	8850 675  8850 1575
Wire Bus Line
	7150 675  7150 1375
Wire Bus Line
	2575 4450 2575 5050
Wire Bus Line
	6775 1275 6775 5050
Wire Bus Line
	6275 675  6275 1975
Connection ~ 3375 2675
Wire Wire Line
	3375 2675 3375 2775
$EndSCHEMATC
