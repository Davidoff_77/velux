#include <Arduino.h>
#include <SPI.h>
#include <ModbusIP.h>
#include "main.h"
#include <avr/wdt.h>

const int shutter_m_1 = 9;
const int shutter_d_1 = 8;
const int shutter_m_2 = 7;
const int shutter_d_2 = 6;
const int shutter_m_3 = 4;
const int shutter_d_3 = 5;
const int shutter_m_4 = A0;
const int shutter_d_4 = A1;

const int shutter_1_down_cl = 1;
const int shutter_1_up_cl = 2;
const int shutter_2_down_cl = 3;
const int shutter_2_up_cl = 4;
const int shutter_3_down_cl = 5;
const int shutter_3_up_cl = 6;
const int shutter_4_down_cl = 7;
const int shutter_4_up_cl = 8;

const unsigned long modbusTimeout = 1000ul*10*60*10; //10 minutes in ms
ModbusIP mb;

void(* resetFunc) (void) = 0;
unsigned long modbusONTIme;

void setup() {    
  // put your setup code here, to run once:
  Serial.begin(9600);
  Ethernet.init(10);
  
  byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};

  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  Serial.println("Begin");

  mb.config(mac);

  Serial.print("Modbus server address:");
  Serial.println(Ethernet.localIP());

  mb.addCoil(shutter_1_up_cl);
  mb.addCoil(shutter_1_down_cl);
  mb.addCoil(shutter_2_up_cl);
  mb.addCoil(shutter_2_down_cl);
  mb.addCoil(shutter_3_up_cl);
  mb.addCoil(shutter_3_down_cl);
  mb.addCoil(shutter_4_up_cl);
  mb.addCoil(shutter_4_down_cl);

  mb.Coil(shutter_1_up_cl, false);
  mb.Coil(shutter_1_down_cl, false);
  mb.Coil(shutter_2_up_cl, false);
  mb.Coil(shutter_2_down_cl, false);
  mb.Coil(shutter_3_up_cl, false);
  mb.Coil(shutter_3_down_cl, false);
  mb.Coil(shutter_4_up_cl, false);
  mb.Coil(shutter_4_down_cl, false);
  
  pinMode(shutter_m_1, OUTPUT);
  pinMode(shutter_d_1, OUTPUT);
  pinMode(shutter_m_2, OUTPUT);
  pinMode(shutter_d_2, OUTPUT);
  pinMode(shutter_m_3, OUTPUT);
  pinMode(shutter_d_3, OUTPUT);
  pinMode(shutter_m_4, OUTPUT);
  pinMode(shutter_d_4, OUTPUT); 

  wdt_enable(WDTO_8S);

  modbusONTIme = millis();
}

void loop() {
  mb.task();

  // Shutter #1
  // Direction
  if(mb.Coil (shutter_1_up_cl))
  {        
    digitalWrite(shutter_d_1, HIGH);
    Serial.println("shutter 1 direction is UP");
  }
  else
  {
    digitalWrite(shutter_d_1, LOW);    
    Serial.println("shutter 1 direction is DOWN");
  }

  // Motor
  if(mb.Coil (shutter_1_up_cl) || mb.Coil (shutter_1_down_cl))
  {
    digitalWrite(shutter_m_1, HIGH);
    Serial.println("shutter 1 motor is ON");
  }
  else
  {
    digitalWrite(shutter_m_1, LOW);
  }

  // Shutter #2
  // Direction
  if(mb.Coil (shutter_2_up_cl))
  {        
    digitalWrite(shutter_d_2, HIGH);
    Serial.println("shutter 2 direction is UP");
  }
  else
  {
    digitalWrite(shutter_d_2, LOW);    
    Serial.println("shutter 2 direction is DOWN");
  }

  // Motor
  if(mb.Coil (shutter_2_up_cl) || mb.Coil (shutter_2_down_cl))
  {
    digitalWrite(shutter_m_2, HIGH);
    Serial.println("shutter 2 motor is ON");
  }
  else
  {
    digitalWrite(shutter_m_2, LOW);
  }

  // Shutter #3
  // Direction
  if(mb.Coil (shutter_3_up_cl))
  {        
    digitalWrite(shutter_d_3, HIGH);
    Serial.println("shutter 3 direction is UP");
  }
  else
  {
    digitalWrite(shutter_d_3, LOW);    
    Serial.println("shutter 3 direction is DOWN");
  }

  // Motor
  if(mb.Coil (shutter_3_up_cl) || mb.Coil (shutter_3_down_cl))
  {
    digitalWrite(shutter_m_3, HIGH);
    Serial.println("shutter 3 motor is ON");
  }
  else
  {
    digitalWrite(shutter_m_3, LOW);
  }

  // Shutter #4
  // Direction
  if(mb.Coil (shutter_4_up_cl))
  {        
    digitalWrite(shutter_d_4, HIGH);
    Serial.println("shutter 4 direction is UP");
  }
  else
  {
    digitalWrite(shutter_d_4, LOW);    
    Serial.println("shutter 4 direction is DOWN");
  }

  // Motor
  if(mb.Coil (shutter_4_up_cl) || mb.Coil (shutter_4_down_cl))
  {
    digitalWrite(shutter_m_4, HIGH);
    Serial.println("shutter 4 motor is ON");
  }
  else
  {
    digitalWrite(shutter_m_4, LOW);
  }

  if( mb.Coil (shutter_1_up_cl) || mb.Coil (shutter_1_down_cl) ||
      mb.Coil (shutter_2_up_cl) || mb.Coil (shutter_2_down_cl) ||
      mb.Coil (shutter_3_up_cl) || mb.Coil (shutter_3_down_cl) ||
      mb.Coil (shutter_4_up_cl) || mb.Coil (shutter_4_down_cl)
  )
  {
    if(millis() > (modbusONTIme + modbusTimeout))
    {
      resetFunc();
    }    
  }

  if( !mb.Coil (shutter_1_up_cl) && !mb.Coil (shutter_1_down_cl) &&
      !mb.Coil (shutter_2_up_cl) && !mb.Coil (shutter_2_down_cl) &&
      !mb.Coil (shutter_3_up_cl) && !mb.Coil (shutter_3_down_cl) &&
      !mb.Coil (shutter_4_up_cl) && !mb.Coil (shutter_4_down_cl)
  )
  {
      modbusONTIme = millis();
  }
  
  wdt_reset();
}